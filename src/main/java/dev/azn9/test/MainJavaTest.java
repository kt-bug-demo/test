package dev.azn9.test;

import dev.azn9.testKotlin.MainKT;
import dev.azn9.testJava.MainJava;

public class MainJavaTest {

    public void testDependency() {
        MainKT mainKT = new MainKT();

        mainKT.test();

        MainJava mainJava = new MainJava();

        mainJava.test();
    }

}
